/*
 *	Zeroxing Implementation
 */
#include "zeroxing.h"

void zxInit( ZeroXing* pZX )
{
	//	Let TMR3 control its i/o pin
	*GPIO_FUNC_SEL0 |= 0x00010000;	// GPIO08

	//	setup zero crossing detector
	*TMR0_SCTRL		= 0;		// External pin configured as an input
	*TMR0_CSCTRL	= 0x0041;	// Interrupt on compare
	*TMR0_LOAD		= 0;		// reload to zero
	*TMR0_COMP1		= 0;		// Interrupt on each input transition
	*TMR0_COMP2		= 0;
	*TMR0_CMPLD1	= 0;
	*TMR0_CMPLD2	= 0;
	*TMR0_CNTR		= 0;		// reset count register
	*TMR0_CTRL		= (TMR0_COUNT_MODE<<13) | (TMR0_PRIME_SRC<<9) | (TMR0_SEC_SRC<<7) | (TMR0_ONCE<<6) | 
					(TMR0_LEN<<5) | (TMR0_DIR<<4) | (TMR0_CO_INIT<<3) | (TMR0_OUT_MODE);

	pZX->cnt = 0;
}

