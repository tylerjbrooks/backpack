/*
 *	Zeroxing Definition
 */
#include "contiki.h"
#include "mc1322x.h"
#include "gpio.h"

#ifndef ZEROXING
#define ZEROXING
 
#define TMR0_COUNT_MODE 1		// Count rising edges of primary source
#define TMR0_PRIME_SRC  0		// Counter 0 input pin
#define TMR0_SEC_SRC    0		// Don't need this
#define TMR0_ONCE       0		// Keep counting
#define TMR0_LEN        1		// Count until compare then reload with value in LOAD
#define TMR0_DIR        0		// Count up
#define TMR0_CO_INIT    0		// Other counters cannot force a re-initialization of this counter
#define TMR0_OUT_MODE   0		// Asserted while counter is active
#define ZXWAITFORRELAY	10		// Wait this number of cycles after the relay closes
typedef struct _ZeroXing
{
	volatile uint32_t cnt;
} ZeroXing;
void zxInit( ZeroXing* pZX );

#endif

