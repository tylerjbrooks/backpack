## Backpack

Backpack is the controller program for an outlet based on the [MC13224](https://www.nxp.com/products/no-longer-manufactured/2.4-ghz-802.15.4-rf-and-32-bit-arm7-mcu-with-128kb-flash-96kb-ram:MC13224V) from FreeScale.  This controller allows the outlet to be controlled via a COaP client.  The outlet can be turned on and off (via a relay) and it can report its energy consumption.  Backpack is written on top of the [Contiki OS](https://github.com/contiki-os/contiki) which provides wireless IPv6 internet access via low power area networks.  Contiki provides multithreading.

### Installation

- todo: HowTo Install the Contiki Environment.
- todo: HowTo Install MC13224 cross compiler.

### Build Notes

Build Backpack:
```
cd your/workspace
cd backpack
make
```


### Usage

Use a USB serial programmmer to flash Backpack into the ROM of the chip.  Release 'reset' and the controller will boot up to normal operation.


### Discussion

This Contiki OS provides an IPv6 stack.  It also implements [protothreads](http://dunkels.com/adam/pt/) which are used to implement the wireless stack.  Contiki was ported to the MC13224 which supports 802.15 type wireless networks.

Files:
- backpack.{h,c}: Main program file.  Establishes main application process.
- itd.{h,c}: Energy measurement controller.
- indicator.{h,c}:  Reset and ON/OFF indicators
- relay.{h,c}: Main relay controller.
- zeroxing.{h,c}: Zero crossing detector.

### To Do

