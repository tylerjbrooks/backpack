ifndef TARGET
TARGET=lowpan-backpack
endif

CONTIKI_PROJECT = backpack
all: $(CONTIKI_PROJECT)

PROJECT_SOURCEFILES += indicator.c relay.c idt.c zeroxing.c

CONTIKI = ../../contiki
CFLAGS += -DPROJECT_CONF_H=\"project-conf.h\"

# for static routing, if enabled
ifneq ($(TARGET), minimal-net)
PROJECT_SOURCEFILES += static-routing.c
endif

# variable for root Makefile.include
WITH_UIP6=1
# for some platforms
UIP_CONF_IPV6=1

# variable for this Makefile
# configure CoAP implementation (3|6|7)
WITH_COAP=7

# must be CFLAGS not variables
# minimal-net does not support RPL, avoid redefine warnings
ifneq ($(TARGET), minimal-net)
CFLAGS += -DUIP_CONF_IPV6_RPL=1
endif

# linker optimizations
SMALL=1

# REST framework, requires WITH_COAP
ifeq ($(WITH_COAP), 7)
${info INFO: compiling with CoAP-07}
CFLAGS += -DWITH_COAP=7
CFLAGS += -DREST=coap_rest_implementation
CFLAGS += -DUIP_CONF_TCP=0
APPS += er-coap-07
else ifeq ($(WITH_COAP), 6)
${info INFO: compiling with CoAP-06}
CFLAGS += -DWITH_COAP=6
CFLAGS += -DREST=coap_rest_implementation
CFLAGS += -DUIP_CONF_TCP=0
APPS += er-coap-06
else ifeq ($(WITH_COAP), 3)
${info INFO: compiling with CoAP-03}
CFLAGS += -DWITH_COAP=3
CFLAGS += -DREST=coap_rest_implementation
CFLAGS += -DUIP_CONF_TCP=0
APPS += er-coap-03
else
${info INFO: compiling with HTTP}
CFLAGS += -DWITH_HTTP
CFLAGS += -DREST=http_rest_implementation
CFLAGS += -DUIP_CONF_TCP=1
APPS += rest-http-engine
endif

APPS += erbium

include $(CONTIKI)/Makefile.include
