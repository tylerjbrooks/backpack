/*
 *	Indicator Definition
 */
#include "contiki.h"
#include "gpio.h"

#ifndef INDICATOR
#define INDICATOR

#define INDICATORLED		(1ULL << 36)	//	GPIO36
#define INDICATORNONE		0ULL
#define INDICATORMODESOLID	0
#define INDICATORMODEBLINK	1
#define INDICATORSTATEOFF	0
#define INDICATORSTATEON	1
#define INDICATORBLINKDELAY	500
typedef struct _Indicator
{
	int32_t mode;
	int32_t state;
	uint32_t blink;
} Indicator;
void indiInit( Indicator* pIndi );
int32_t indiGetMode( Indicator* pIndi );
void indiSetMode( Indicator* pIndi, int32_t mode );
int32_t indiGetState( Indicator* pIndi );
void indiSetState( Indicator* pIndi, int32_t state );
void indiToggleState( Indicator* pIndi );
void indiBlink( Indicator* pIndi );

#endif
