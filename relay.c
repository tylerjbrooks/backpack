/*
 *	Relay Implementation
 */
#include "relay.h" 

void rlyInit( Relay* pRelay )
{
	//	relay gpio pin is output
	gpio_pad_dir_set( RELAYGPIOBITSET | RELAYGPIOBITRESET );
	
	//	no relay state yet
	gpio_data_reset( RELAYGPIOBITSET | RELAYGPIOBITRESET );
	pRelay->cnt = 0;
	pRelay->state = RELAYSTATEUNKNOWN;
}
int32_t rlyGetState( Relay* pRelay )
{
	return pRelay->state;
}
void rlySetState( Relay* pRelay, int32_t state )
{	
	pRelay->state = state;
}
void rlyAdvanceState( Relay* pRelay )
{
	switch( pRelay->state )
	{
	//	Opening the relay.
	//	Turn on the 'reset' coil driver for 'cnt' cycles.
	//	State Machine:  REQUESTOPEN -> OPENING -> OPEN
	case RELAYSTATEREQUESTOPEN:
		gpio_data_set( RELAYGPIOBITRESET );
		pRelay->state = RELAYSTATEOPENING;
		pRelay->cnt = 0;
		break;
	case RELAYSTATEOPENING:
		pRelay->cnt += 1;
		if( pRelay->cnt >= RELAYREQUESTCNT )
		{
			gpio_data_reset( RELAYGPIOBITSET | RELAYGPIOBITRESET );
			pRelay->state = RELAYSTATEOPEN;
			pRelay->cnt = 0;
		}
		break;
	case RELAYSTATEOPEN:
		break;
		
	
	//	Closing the relay.
	//	Turn on the 'set' coil driver for 'cnt' cylces
	//	State Machine:  REQUESTCLOSED -> CLOSING -> CLOSED
	case RELAYSTATEREQUESTCLOSED:
		gpio_data_set( RELAYGPIOBITSET );
		pRelay->state = RELAYSTATECLOSING;
		pRelay->cnt = 0;
		break;
	case RELAYSTATECLOSING:
		pRelay->cnt += 1;
		if( pRelay->cnt >= RELAYREQUESTCNT )
		{
			gpio_data_reset( RELAYGPIOBITSET | RELAYGPIOBITRESET );
			pRelay->state = RELAYSTATECLOSED;
			pRelay->cnt = 0;
		}
		break;
	case RELAYSTATECLOSED:
		break;
		
		
	//	State Unknown.  
	//	Do Nothing.
	case RELAYSTATEUNKNOWN:	
	default:
		break;
	}
}
int32_t rlyRequest( Relay* pRelay, int32_t state )
{
	if( rlyIsRequestable( pRelay ) )
	{
		rlySetState( pRelay, state );
		return 1;
	}
	return 0;
}
int32_t rlyRequestOpen( Relay* pRelay )
{
	return rlyRequest( pRelay, RELAYSTATEREQUESTOPEN );
}
int32_t rlyRequestClosed( Relay* pRelay )
{
	return rlyRequest( pRelay, RELAYSTATEREQUESTCLOSED );
}


