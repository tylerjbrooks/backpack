/*
 *	Relay Definition
 */
#include "contiki.h"
#include "gpio.h"

#ifndef RELAY
#define RELAY

#define RELAYGPIOBITSET		(1ULL << 32) //	GPIO32
#define RELAYGPIOBITRESET	(1ULL << 33) //	GPIO33
#define RELAYGPIOBITNONE	0ULL

#define RELAYSTATEUNKNOWN		0
#define RELAYSTATEREQUESTOPEN	1
#define RELAYSTATEREQUESTCLOSED	2
#define RELAYSTATEOPENING		3
#define RELAYSTATECLOSING		4
#define RELAYSTATEOPEN			5
#define RELAYSTATECLOSED		6
#define RELAYREQUESTCNT			5

#define rlyIsUnknown(x)			((rlyGetState(x))==RELAYSTATEUNKNOWN)
#define rlyIsOpen(x)			((rlyGetState(x))==RELAYSTATEOPEN)
#define rlyIsClosed(x)			((rlyGetState(x))==RELAYSTATECLOSED)
#define rlyIsTerminal(x)		(rlyIsOpen(x)||rlyIsClosed(x))
#define rlyIsRequestable(x)		(rlyIsTerminal(x)||rlyIsUnknown(x))
#define rlyIsRequestOpen(x)		((rlyGetState(x))==RELAYSTATEREQUESTOPEN)
#define rlyIsRequestClosed(x)	((rlyGetState(x))==RELAYSTATEREQUESTCLOSED)
#define rlyIsRequest(x)			(rlyIsRequestOpen(x)||rlyIsRequestClosed(x))

typedef struct _Relay
{
	volatile int32_t cnt;
	volatile int32_t state;
} Relay;
void rlyInit( Relay* pRelay );
int32_t rlyGetState( Relay* pRelay );
void rlySetState( Relay* pRelay, int32_t state );
void rlyAdvanceState( Relay* pRelay );

int32_t rlyRequest( Relay* pRelay, int32_t state );
int32_t rlyRequestOpen( Relay* pRelay );
int32_t rlyRequestClosed( Relay* pRelay );

#endif

