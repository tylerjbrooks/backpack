/*
 *	Indicator Implementation
 */
#include "indicator.h"

void indiInit( Indicator* pIndi )
{
	//	indicator drivers are output pins
	gpio_pad_dir_set( INDICATORLED );

	//	turn on indicator light
	gpio_data_set( INDICATORLED );

	//	indicator are solid (not blinking)
	pIndi->mode = INDICATORMODESOLID;
	pIndi->state = INDICATORSTATEON;
	pIndi->blink = 0;
}
int32_t indiGetMode( Indicator* pIndi )
{
	return pIndi->mode;
}
void indiSetMode( Indicator* pIndi, int32_t mode )
{
	if( pIndi->mode != mode )
	{
		pIndi->mode = mode;
		if( mode == INDICATORMODEBLINK )
		{
			pIndi->blink = 0;
			indiSetState( pIndi, INDICATORSTATEOFF );
		}
		else
		{
			indiSetState( pIndi, INDICATORSTATEON );
		}
	}
}
int32_t indiGetState( Indicator* pIndi )
{
	return pIndi->state;
}
void indiSetState( Indicator* pIndi, int32_t state )
{
	pIndi->state = state;
	if( state == INDICATORSTATEOFF )
		gpio_data_reset( INDICATORLED );
	else
		gpio_data_set( INDICATORLED );
}
void indiToggleState( Indicator* pIndi )
{
	if( indiGetMode( pIndi ) == INDICATORMODEBLINK )
		indiSetState( pIndi, 
			( indiGetState( pIndi ) == INDICATORSTATEON ) ? INDICATORSTATEOFF : INDICATORSTATEON );
}
void indiBlink( Indicator* pIndi )
{
	if( indiGetMode( pIndi ) == INDICATORMODEBLINK )
		indiToggleState( pIndi );
}



