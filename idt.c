/*
 *	IDT Implementation
 */
#include "idt.h"

void idtInit( Idt* pIdt, uint64_t cs )
{
	//	remember the chip select
	pIdt->cs = cs;

	//	CS, CLK and MOSI are output
	gpio_data_set( pIdt->cs | IDTGPIOBITCLK );
	gpio_data_reset( IDTGPIOBITMOSI );
	gpio_pad_dir_set( pIdt->cs | IDTGPIOBITCLK | IDTGPIOBITMOSI );

	//	MISO is input
	gpio_pad_dir_reset( IDTGPIOBITMISO );

	pIdt->addr = 0;
	pIdt->data = 0;

	idtDelay( pIdt, IDTDELAYLONG );
	idtDelay( pIdt, IDTDELAYLONG );
	idtDelay( pIdt, IDTDELAYLONG );
}
uint64_t idtGetCS( Idt* pIdt )
{
	return pIdt->cs;
}
void idtDelay( Idt* pIdt, uint32_t time )
{
	for( pIdt->delay = 0; pIdt->delay < time; pIdt->delay++ ) 
	{ 
		continue; 
	}
}
void idtClkOutAddr( Idt* pIdt, uint8_t addr )
{
	int32_t i;
	pIdt->addr = addr;
	
	for( i = 0; i < 8; i++ )
	{
		//	clock down
		gpio_data_reset( IDTGPIOBITCLK );

		idtDelay( pIdt, IDTDELAYSHORT );

		//	set/reset output bit
		if( addr & 0x80 )
			gpio_data_set( IDTGPIOBITMOSI );
		else 
			gpio_data_reset( IDTGPIOBITMOSI );

		idtDelay( pIdt, IDTDELAYSHORT );

		//	clock up
		gpio_data_set( IDTGPIOBITCLK );

		idtDelay( pIdt, IDTDELAYSHORT );
		idtDelay( pIdt, IDTDELAYSHORT );

		//	position next bit
		addr <<= 1;
	}
}
void idtClkOutData( Idt* pIdt, uint16_t data )
{
	int32_t i;
	pIdt->data = data;
	
	for( i = 0; i < 16; i++ )
	{
		//	clock down
		gpio_data_reset( IDTGPIOBITCLK );

		idtDelay( pIdt, IDTDELAYSHORT );

		//	set/reset output bit
		if( data & 0x8000 )
			gpio_data_set( IDTGPIOBITMOSI );
		else 
			gpio_data_reset( IDTGPIOBITMOSI );

		idtDelay( pIdt, IDTDELAYSHORT );

		//	clock up
		gpio_data_set( IDTGPIOBITCLK );

		idtDelay( pIdt, IDTDELAYSHORT );
		idtDelay( pIdt, IDTDELAYSHORT );

		//	position next bit
		data <<= 1;
	}
}
void idtClkInData( Idt* pIdt, uint16_t* pData )
{
	int32_t i;
	uint16_t data = 0;
	
	for( i = 0; i < 16; i++ )
	{
		uint64_t d;

		//	clock down
		gpio_data_reset( IDTGPIOBITCLK );

		idtDelay( pIdt, IDTDELAYSHORT );

		//	set bit if input is high
		d = gpio_data_get( IDTGPIOBITMISO );
		if( d & IDTGPIOBITMISO )
			data = (data | 0x1ULL);

		idtDelay( pIdt, IDTDELAYSHORT );

		//	clock up
		gpio_data_set( IDTGPIOBITCLK );

		idtDelay( pIdt, IDTDELAYSHORT );
		idtDelay( pIdt, IDTDELAYSHORT );

		//	position next bit
		if( i != 15 )
			data <<= 1;
	}

	*pData = data;
	pIdt->data = data;
}
int32_t idtWrite( Idt* pIdt, uint8_t addr, uint16_t data )
{
	pIdt->addr = addr;
	pIdt->data = data;

	//	select chip
	gpio_data_reset( pIdt->cs );

	idtDelay( pIdt, IDTDELAYLONG );

	//	clock out address
	//	magic:  when writing, high bit of Addr is clear!
	idtClkOutAddr( pIdt, addr );

	//	clock out data
	idtClkOutData( pIdt, data );

	idtDelay( pIdt, IDTDELAYLONG );

	//	deselect chip
	gpio_data_set( pIdt->cs );

	idtDelay( pIdt, IDTDELAYLONG );
	idtDelay( pIdt, IDTDELAYLONG );

	return 1;
}
int32_t idtRead( Idt* pIdt, uint8_t addr, uint16_t* pData )
{
	pIdt->addr = addr;

	//	select chip
	gpio_data_reset( pIdt->cs );

	idtDelay( pIdt, IDTDELAYLONG );

	//	clock out address
	//	magic:  when reading, high bit of Addr is set!
	idtClkOutAddr( pIdt, addr | 0x80 );

	//	clock in data
	idtClkInData( pIdt, pData );

	idtDelay( pIdt, IDTDELAYLONG );

	//	deselect chip
	gpio_data_set( pIdt->cs );

	pIdt->data = *pData;

	idtDelay( pIdt, IDTDELAYLONG );
	idtDelay( pIdt, IDTDELAYLONG );

	return 1;
}
int32_t idtVerify( Idt* pIdt, uint16_t data )
{
	uint16_t read;
	idtRead( pIdt, IDTCMDADDRLASTSPIDATA, &read );
//	PRINTF("Verify = 0x%x, 0x%x\n", (int)data, (int)read );
	return read == data;
}
int32_t idtGetRegister( Idt* pIdt, uint8_t reg, uint16_t* pData )
{
//	PRINTF( "IDT Get Reg (%x): ", (int)reg );
	return idtRead( pIdt, reg, pData ) && idtVerify( pIdt, *pData );
}
int32_t idtSetRegister( Idt* pIdt, uint8_t reg, uint16_t data )
{
//	PRINTF( "IDT Set Reg (%x): ", (int)reg );
	return idtWrite( pIdt, reg, data ) && idtVerify( pIdt, data );
}



