/*
 *	Outlet Implementation
 */

#include <stdio.h>
#include <string.h>
#include "contiki.h"
#include "contiki-net.h"
#include "mc1322x.h"
#include "etimer.h"
#include "sensors.h"

#include "backpack.h"
#include "indicator.h"
#include "relay.h"
#include "idt.h"
#include "zeroxing.h"

#include "erbium.h"

//	Define this to get tenth watt accuracy
//#define TENTHWATTACCURACY 1

/* For CoAP-specific example: not required for normal RESTful Web service. */
#if WITH_COAP == 3
#include "er-coap-03.h"
#elif WITH_COAP == 6
#include "er-coap-06.h"
#elif WITH_COAP == 7
#include "er-coap-07.h"
#else
#warning "REST example without CoAP"
#endif /* CoAP-specific example */

/*
 * Global Data
 */
static Indicator indi;
static Relay relay;
static ZeroXing zeroxing;
static Idt idtA;		//	controlled socket
static Idt idtB;		//	uncontrolled socket

static struct etimer reportTimer;

/*
 * Interrupts
 */
#define zeroxing_isr	tmr0_isr
void zeroxing_isr( void ) 
{
	uint32_t x = *TMR0_CSCTRL;
	if( (x & 0x0010) != 0 )
	{
		//	Advance the state of the relay.
		//	This will open/close the relay.
		rlyAdvanceState( &relay );

		*TMR0_SCTRL = 0;
		*TMR0_CSCTRL = 0x0041;
	}
}

/*
 * Application
 */
static void setupIDT( Idt* pIdt )
{
	uint16_t idtData;

	//	IDT reset -> start cal -> write cal -> end cal -> normal metering...
	PRINTF( "Metering......" );
	idtSetRegister( pIdt, IDTCMDADDRSOFTRESET, IDTCMDSOFTRESET );
	idtGetRegister( pIdt, IDTCMDADDRCALSTART, &idtData );
	idtSetRegister( pIdt, IDTCMDADDRCALSTART, IDTCMDCALSTART_STARTUP );
	idtSetRegister( pIdt, IDTCMDADDRPLCONSTH, 0x0025 );
	idtSetRegister( pIdt, IDTCMDADDRPLCONSTL, 0xdddd );
#ifdef TENTHWATTACCURACY
	idtSetRegister( pIdt, IDTCMDADDRMMODE, 0x7c22 );	// x24 gain
#else
	idtSetRegister( pIdt, IDTCMDADDRMMODE, 0x1c22 );	// x4 gain
#endif
	idtSetRegister( pIdt, IDTCMDADDRLGAIN, 0x1475 );
	idtSetRegister( pIdt, IDTCMDADDRLPHI, 0x0024 );
	idtSetRegister( pIdt, IDTCMDADDRPNOLTH, 0x08bd );
	idtGetRegister( pIdt, IDTCMDADDRCS1, &idtData );
	idtSetRegister( pIdt, IDTCMDADDRCS1, idtData );
	idtSetRegister( pIdt, IDTCMDADDRCALSTART, IDTCMDCALSTART_NORMAL );
	idtGetRegister( pIdt, IDTCMDADDRSYSSTATUS, &idtData );
	PRINTF( "Status = 0x%x\n", idtData );

	PRINTF( "Measurement..." );
	idtGetRegister( pIdt, IDTCMDADDRADJSTART, &idtData );
	idtSetRegister( pIdt, IDTCMDADDRADJSTART, IDTCMDADJSTART_STARTUP );
	if( idtGetCS( pIdt ) == IDTGPIOBITCSA )
	{
		idtSetRegister( pIdt, IDTCMDADDRUGAIN, 0x4E60 );
#ifdef TENTHWATTACCURACY
		idtSetRegister( pIdt, IDTCMDADDRIGAINL, 0x83AB );	//	1.667x gain
#else
		idtSetRegister( pIdt, IDTCMDADDRIGAINL, 0x829F );	//	1x gain
#endif
		idtSetRegister( pIdt, IDTCMDADDRUOFFSET, 0x0040 );
		idtSetRegister( pIdt, IDTCMDADDRIOFFSETL, 0xF800 );
	}	else
	{
		idtSetRegister( pIdt, IDTCMDADDRUGAIN, 0x4E60 );
#ifdef TENTHWATTACCURACY
		idtSetRegister( pIdt, IDTCMDADDRIGAINL, 0x8840 );	//	1.667x gain
#else
		idtSetRegister( pIdt, IDTCMDADDRIGAINL, 0x829F );	//	1x gain
#endif
		idtSetRegister( pIdt, IDTCMDADDRUOFFSET, 0x0040 );
		idtSetRegister( pIdt, IDTCMDADDRIOFFSETL, 0xF800 );
	}
	
	idtGetRegister( pIdt, IDTCMDADDRCS2, &idtData );
	idtSetRegister( pIdt, IDTCMDADDRCS2, idtData );
	idtSetRegister( pIdt, IDTCMDADDRADJSTART, IDTCMDADJSTART_NORMAL );
	idtGetRegister( pIdt, IDTCMDADDRSYSSTATUS, &idtData );
	PRINTF( "Status = 0x%x\n", idtData );
	
	return;
}
static char* pVrms = "Vrms";
static char* pIrms = "Irms";
static char* pFreq = "Freq";
static char* pPmean = "Pmean";
static char* pSmean = "Smean";
static char* pPfactor = "Pfactor";
static char* pPangle = "Pangle";
static char* pAll = "All";
static char* getItemIDT( Idt* pIdt, const char* pItem, char* pResult )
{
	uint16_t data;
	
	if( strcmp( pItem, pVrms ) == 0 ) 
	{  
		idtGetRegister( pIdt, IDTCMDADDRURMS, &data );
		sprintf( pResult, "%s=%d.%02dV", pVrms, ((int)data)/100, ((int)data)%100 );
		return pResult;
	}
	else if( strcmp( pItem, pIrms ) == 0 ) 
	{  
		idtGetRegister( pIdt, IDTCMDADDRIRMS, &data );
#ifdef TENTHWATTACCURACY
		sprintf( pResult, "%s=%d.%04dA", pIrms, ((int)data)/10000, ((int)data)%10000 );
#else
		sprintf( pResult, "%s=%d.%03dA", pIrms, ((int)data)/1000, ((int)data)%1000 );
#endif
		return pResult;
	}
	else if( strcmp( pItem, pFreq ) == 0 ) 
	{  
		idtGetRegister( pIdt, IDTCMDADDRFREQ, &data );
		sprintf( pResult, "%s=%d.%02dHz", pFreq, ((int)data)/100, ((int)data)%100 );
		return pResult;
	}
	else if( strcmp( pItem, pPmean ) == 0 ) 
	{  
		idtGetRegister( pIdt, IDTCMDADDRPMEAN, &data );
#ifdef TENTHWATTACCURACY
		sprintf( pResult, "%s=%d.%04dkVA", pPmean, ((int)data)/10000, ((int)data)%10000 );
#else
		sprintf( pResult, "%s=%d.%03dkVA", pPmean, ((int)data)/1000, ((int)data)%1000 );
#endif
		return pResult;
	}
	else if( strcmp( pItem, pSmean ) == 0 ) 
	{  
		idtGetRegister( pIdt, IDTCMDADDRSMEAN, &data );
#ifdef TENTHWATTACCURACY
		sprintf( pResult, "%s=%d.%04dkVA", pSmean, ((int)data)/10000, ((int)data)%10000 );
#else
		sprintf( pResult, "%s=%d.%03dkVA", pSmean, ((int)data)/1000, ((int)data)%1000 );
#endif
		return pResult;
	}
	else if( strcmp( pItem, pPfactor ) == 0 ) 
	{  
		idtGetRegister( pIdt, IDTCMDADDRPOWERF, &data );
		sprintf( pResult, "%s=%d.%03d", pPfactor, ((int)data)/1000, ((int)data)%1000 );
		return pResult;
	}
	else if( strcmp( pItem, pPangle ) == 0 ) 
	{  
		idtGetRegister( pIdt, IDTCMDADDRPANGLE, &data );
		sprintf( pResult, "%s=%d.%01d", pPangle, ((int)data)/10, ((int)data)%10 );
		return pResult;
	}
	else if( strcmp( pItem, pAll ) == 0 )
	{
		char tmp[128];
		char* space = " ";
		tmp[0] = 0;
		strcpy( pResult, getItemIDT( pIdt, pVrms, tmp ) );
		strcat( pResult, space );
		strcat( pResult, getItemIDT( pIdt, pIrms, tmp ) );
		strcat( pResult, space );
		strcat( pResult, getItemIDT( pIdt, pFreq, tmp ) );
		strcat( pResult, space );
		strcat( pResult, getItemIDT( pIdt, pPmean, tmp ) );
		strcat( pResult, space );
		strcat( pResult, getItemIDT( pIdt, pSmean, tmp ) );
		strcat( pResult, space );
		strcat( pResult, getItemIDT( pIdt, pPfactor, tmp ) );
		strcat( pResult, space );
		strcat( pResult, getItemIDT( pIdt, pPangle, tmp ) );
		strcat( pResult, space );
	}
	return NULL;
}
static void reportSwitch( Relay* pRelay )
{
	char msg[50];
	msg[0] = 0;
	sprintf( msg, "Switch=%s\n", rlyIsOpen( pRelay ) ? "open" : "closed" );
	PRINTF( msg );
}
static void reportIDT( Idt* pIdt )
{
	uint16_t data;
	uint32_t cs;
	char msg[128];
	msg[0] = 0;
	strcpy( msg, idtGetCS( pIdt ) == IDTGPIOBITCSA ? "A) " : "B) " );
	getItemIDT( pIdt, pAll, msg + strlen( msg ) );
	strcat( msg, "\n" );
	PRINTF( msg );
}

RESOURCE(switch, METHOD_GET | METHOD_POST | METHOD_PUT, "switch", "title=\"Switch: ?set=xxx\";rt=\"Text\"");
void switch_handler( void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset )
{
	int length;
	const char* set;
	char msg[REST_MAX_CHUNK_SIZE];
	int opening = 0;
	
	opening = rlyIsOpen( &relay ) ? 1 : 0;
	
	if( (length=REST.get_query_variable( request, "set", &set )) )
	{
		char tmp[25];
		memset( tmp, 0, sizeof( tmp ) );
		memcpy( tmp, set, length );
		if( strcmp( tmp, "open" ) == 0 )
		{
			opening = 1;
			rlyRequestOpen( &relay );
			indiSetState( &indi, INDICATORSTATEOFF );
		}
		else if( strcmp( tmp, "closed" ) == 0 )
		{
			opening = 0;
			rlyRequestClosed( &relay );
			indiSetState( &indi, INDICATORSTATEON );
		}
		else if( strcmp( tmp, "toggle" ) == 0 )
		{
			if( rlyIsOpen( &relay ) )
			{
				opening = 0;
				rlyRequestClosed( &relay );
				indiSetState( &indi, INDICATORSTATEON );
			}
			else
			{
				opening = 1;
				rlyRequestOpen( &relay );
				indiSetState( &indi, INDICATORSTATEOFF );
			}
		}
	}

	memset( msg, 0, sizeof( msg ) );
	if( opening )
		strcpy( msg, "Switch=open\n" );
	else
		strcpy( msg, "Switch=closed\n" );
	
	memcpy( buffer, msg, length = strlen( msg ) );

	REST.set_header_content_type( response, REST.type.TEXT_PLAIN ); 
	REST.set_header_etag( response, (uint8_t*)&length, 1 );
	REST.set_response_payload( response, buffer, length );
}
RESOURCE(measure, METHOD_GET | METHOD_POST | METHOD_PUT, "measure", "title=\"Measure: ?channel=A|B&item=Vrms\";rt=\"Text\"");
void measure_handler( void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset )
{
	const char *buf = NULL;
	char *notrecognized = "Item Not Recognized";
	int length;
	Idt* pIdt = &idtA;
	
	//	Assume channel 'A' unless specified
	if( (length=REST.get_query_variable( request, "channel", &buf )) )
	{
		if( buf[0] == 'B' )
			pIdt = &idtB;
	}
	
	if( (length=REST.get_query_variable( request, "item", &buf )) )
	{
		char msg[128];
		char item[20];
		memset( msg, 0, sizeof( msg ) );
		memset( item, 0, sizeof( item ) );
		memcpy( item, buf, length );
		
		strcpy( msg, idtGetCS( pIdt ) == IDTGPIOBITCSA ? "A) " : "B) " );
		getItemIDT( pIdt, item, msg + strlen( msg ) );
		strcat( msg, "\n" );
		
		length = strlen( msg + *offset );
		if( length > preferred_size )
		{
			length = preferred_size;
			memcpy( buffer, msg + *offset, preferred_size );
			*offset += length;
		}
		else
		{
			memcpy( buffer, msg + *offset, length );
			*offset = -1;	
		}
	}
	else
	{
		memcpy( buffer, notrecognized, strlen( notrecognized )+1 );
	}

	REST.set_header_content_type( response, REST.type.TEXT_PLAIN ); 
	REST.set_header_etag( response, (uint8_t *) &length, 1 );
	REST.set_response_payload( response, buffer, length );
}


#define RESETLIGHT		(1ULL << 30) //	GPIO30
static int resetcount;
static struct etimer blinkTimer;

PROCESS( outlet_process, "outlet process" );
AUTOSTART_PROCESSES( &outlet_process );
PROCESS_THREAD( outlet_process, ev, data )
{
	PROCESS_BEGIN();

#ifdef RF_CHANNEL
	PRINTF("RF channel: %u\n", RF_CHANNEL);
#endif
#ifdef IEEE802154_PANID
	PRINTF("PAN ID: 0x%04X\n", IEEE802154_PANID);
#endif

	PRINTF("uIP buffer: %u\n", UIP_BUFSIZE);
	PRINTF("LL header: %u\n", UIP_LLH_LEN);
	PRINTF("IP+UDP header: %u\n", UIP_IPUDPH_LEN);
	PRINTF("REST max chunk: %u\n", REST_MAX_CHUNK_SIZE);

/* if static routes are used rather than RPL */
#if !UIP_CONF_IPV6_RPL && !defined (CONTIKI_TARGET_MINIMAL_NET)
	set_global_address();
	configure_routing();
#endif

	//	Init Subsystems
	indiInit( &indi );
	rlyInit( &relay );
	idtInit( &idtA, IDTGPIOBITCSA );
	idtInit( &idtB, IDTGPIOBITCSB );
	zxInit( &zeroxing );
	rest_init_framework();
	
	//	Setup IDT
	setupIDT( &idtA );
	setupIDT( &idtB );
	
	//	Setup REST resources
	rest_activate_resource( &resource_switch );
	rest_activate_resource( &resource_measure );
	
	//	Wait for the relay to get to a terminal state
	rlyRequestClosed( &relay );
	while( !rlyIsTerminal( &relay ) )
		;
		
	//	Toggle reset light
	resetcount = 20;
	gpio_pad_dir_set( RESETLIGHT );
	gpio_data_set( RESETLIGHT );
	etimer_set( &blinkTimer, CLOCK_SECOND );

	//	Setup Reporting
	etimer_set( &reportTimer, 3*CLOCK_SECOND );
	
	//	Main Application Loop
	while( 1 ) 
	{
		//	Wait for something to happen
		PROCESS_YIELD();
		
		//	Timer Events
		if( ev == PROCESS_EVENT_TIMER ) 
		{
			//	Report Timer
			if( data == &reportTimer && etimer_expired( &reportTimer ) )
			{
				reportSwitch( &relay );
				reportIDT( &idtA );
				reportIDT( &idtB );
				etimer_restart( &reportTimer );	
			}
			else if( data == &blinkTimer && etimer_expired( &blinkTimer ) )
			{
				if( resetcount )
				{
					if( resetcount % 2 )
					{
						gpio_data_set( RESETLIGHT );
					}
					else
					{
						gpio_data_reset( RESETLIGHT );
					}
					resetcount -= 1;
					etimer_restart( &blinkTimer );
				}
				else
				{
					resetcount = 0;
					gpio_data_reset( RESETLIGHT );
					etimer_stop( &blinkTimer );
				}
			}
		}
		
		//	Sensor Events
		else if( ev == sensors_event ) 
		{
			if( ((struct sensors_sensor*)data)->value(0) )
			{
				//	Button pushed.
				//	Toggle the relay and led
				if( rlyIsOpen( &relay ) )
				{
					rlyRequestClosed( &relay );
					indiSetState( &indi, INDICATORSTATEON );
				}
				else
				{
					rlyRequestOpen( &relay );
					indiSetState( &indi, INDICATORSTATEOFF );
				}
			}
		}
	}

	PROCESS_END();
}


